const themeBtn = document.getElementById('theme');
const body = document.querySelector('body');
const header = document.querySelector('header');

const menuBtn = document.querySelector(".menu-btn");
const navOptions = document.querySelector(".nav-options");

const dropList = document.querySelector(".options-container");
const dropBox = document.querySelector(".select-box");
const deviceInfo = document.querySelector(".device-info")

const options = document.querySelectorAll(".option");

const searchBox = document.querySelector(".filter-container input")


// blur-effects
const blurBody = document.querySelector(".main-contents");
const blurBtn = document.querySelector(".device-button");
const blurIcon = document.querySelector(".icon-logo");

themeBtn.onclick = function() {
    themeBtn.classList.toggle('active');
    body.classList.toggle('active');
    header.classList.toggle('active');
}

// Responsive and selection function
menuBtn.addEventListener("click", responsiveBtn);
dropBox.addEventListener("click", dropDownFunc);

function responsiveBtn() {
    menuBtn.classList.toggle('active');
    navOptions.classList.toggle('active');
    
    blurBody.classList.toggle('blur');
    blurBtn.classList.toggle('blur');
    blurIcon.classList.toggle('blur');
    deviceInfo.classList.toggle('blur');
}

function dropDownFunc() {
    dropBox.classList.toggle('active');
    dropList.classList.toggle('active');
    deviceInfo.classList.toggle('visible');
    searchBox.value = "";
    filterList("");

    if (dropList.classList.contains("active")) {
        searchBox.focus();
    }
}

options.forEach( i => {
    i.addEventListener("click", () => {
        dropBox.innerHTML = i.querySelector('label').innerHTML;
        dropList.classList.remove("active");
    });
});

// Filter function
searchBox.addEventListener("keyup", function (e) {
    filteredList(e.target.value);
});

const filteredList = searchTerm => {
    searchTerm = searchTerm.toLowerCase();
    options.forEach(x => {
        let label = x.firstElementChild.nextElementSibling.innerText.toLowerCase();
        if (label.indexOf(searchTerm) > -1) {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    });
};